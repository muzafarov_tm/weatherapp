package com.dev.klvra.weatherapp.model;


public class CityOnFirstList {          //this class contains information about item on first screen

    private String cityName;
    private Integer temperature;

    public CityOnFirstList(String cityName, Integer temperature){
        this.cityName = cityName;
        this.temperature = temperature/10;
    }

    public String getCityName() { return cityName; }
    public void setCityName (String cityName) { this.cityName = cityName; }

    public Integer getTemperature () { return temperature; }
    public void setTemperature (Integer temperature) { this.temperature = temperature; }
}
