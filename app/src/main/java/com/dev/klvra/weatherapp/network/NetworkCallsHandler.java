package com.dev.klvra.weatherapp.network;


import android.content.Context;
import android.util.Log;

import com.dev.klvra.weatherapp.DBCallback;
import com.dev.klvra.weatherapp.WeatherActivity;
import com.dev.klvra.weatherapp.database.DBAccess;
import com.dev.klvra.weatherapp.model.CityDetailedInfo;
import com.dev.klvra.weatherapp.model.CityOnFirstList;
import com.dev.klvra.weatherapp.model.ResponseWeather;
import com.google.gson.Gson;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkCallsHandler {

    public final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private Retrofit retrofit;
    ResponseWeather weather;
    DBAccess dbAccess;
    Context context;
    CityOnFirstList cityOnFirstList;
    CityDetailedInfo cityDetailedInfo;
    DBCallback callback;

    public NetworkCallsHandler(Context context){

        this.context = context;
        this.callback = (DBCallback) context;
    }


    public void fillDB(){

        String[] citiesArray = {"kazan", "moscow", "samara", "krasnodar", "nizhnekamsk"};
        for(int i=0; i<citiesArray.length; i++){
            getWeatherIn(citiesArray[i]);
        }
        callback.onDBFilled(true);
    }

    public void getWeatherIn (String city){
        dbAccess = DBAccess.getInstance(context);
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OpenWeatherInterface api = retrofit.create(OpenWeatherInterface.class);
        String language = Locale.getDefault().getDisplayLanguage();
        String request =BASE_URL + "weather?q=" + city + "&lang=" + language + "&APPID=" + "237ce7c29f557850803f8208b24822fb";
        //Log.d("MyTag",BASE_URL + "weather?q=" + request );
        Call<ResponseWeather> call = api.getWeatherIn(request);
        call.enqueue(new Callback<ResponseWeather>() {
            @Override
            public void onResponse(Call<ResponseWeather> call, Response<ResponseWeather> response) {

                weather = response.body();
                if(response!=null) {
                    cityOnFirstList = new CityOnFirstList(response.body().getName(),
                            response.body().getMain().getTemp().intValue());
                    cityDetailedInfo = new CityDetailedInfo (
                            response.body().getName(),
                            response.body().getMain().getTemp().intValue(),
                            response.body().getMain().getTempMin().intValue(),
                            response.body().getMain().getPressure(),
                            response.body().getMain().getTempMax().intValue(),
                            response.body().getMain().getHumidity(),
                            response.body().getWind().getSpeed().intValue(),
                            response.body().getWind().getDeg().intValue(),
                            response.body().getWeather().get(0).getDescription(),
                            response.body().getWeather().get(0).getIcon());
                    /*Log.d("MyTag", response.body().getName() + " " + response.body().getMain().getTemp());
                    Log.d("MyTag", response.body().getMain().getHumidity() + " ");
                    Log.d("MyTag", response.body().getMain().getPressure() + " ");
                    Log.d("MyTag", response.body().getMain().getTempMin() + " ");
                    Log.d("MyTag", response.body().getMain().getTempMax() + " ");
                    Log.d("MyTag", response.body().getWind().getSpeed() + " " + response.body().getWind().getDeg());
                    Log.d("MyTag", response.body().getWeather().get(0).getDescription());*/
                    dbAccess.open();
                    dbAccess.addOrRefreshInfoInDB(cityDetailedInfo);
                    dbAccess.close();
                }
                else Log.d("MyTag","null response!");
            }

            @Override
            public void onFailure(Call<ResponseWeather> call, Throwable t) {
                Log.d("MyTag","OOPS! " + t.getMessage());
            }
        });
    }
}
