package com.dev.klvra.weatherapp.model;


public class CityDetailedInfo {

    private String cityname;
    private Integer temperature;
    private Integer tempmin;
    private Integer tempmax;
    private Integer pressure;
    private Integer humidity;
    private Integer windSpeed;
    private Integer windDir;
    private String clouds;
    private String image;

    public CityDetailedInfo(String cityname, Integer temperature, Integer tempmin, Integer pressure,
                            Integer tempmax, Integer humidity, Integer windSpeed, Integer windDir, String clouds, String image) {
        this.cityname = cityname;
        this.temperature = temperature;
        this.tempmin = tempmin;
        this.pressure = pressure;
        this.tempmax = tempmax;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDir = windDir;
        this.clouds = clouds;
        this.image = image;
    }

    public String getCityname() { return cityname; }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getTempmin() {
        return tempmin;
    }

    public void setTempmin(Integer tempmin) {
        this.tempmin = tempmin;
    }

    public Integer getTempmax() {
        return tempmax;
    }

    public void setTempmax(Integer tempmax) {
        this.tempmax = tempmax;
    }

    public Integer getPressure() {
        return pressure;
    }

    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public Integer getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Integer windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Integer getWindDir() {
        return windDir;
    }

    public void setWindDir(Integer windDir) {
        this.windDir = windDir;
    }

    public String getClouds() {
        return clouds;
    }

    public void setClouds(String clouds) {
        this.clouds = clouds;
    }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }
}
