package com.dev.klvra.weatherapp;

public interface DBCallback {
    public void onDBFilled(boolean isFilled);
}
