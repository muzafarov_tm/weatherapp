package com.dev.klvra.weatherapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import com.dev.klvra.weatherapp.database.DBAccess;
import com.dev.klvra.weatherapp.fragments.CitiesListFragment;
import com.dev.klvra.weatherapp.fragments.InfoAboutWeatherFragment;
import com.dev.klvra.weatherapp.model.CityDetailedInfo;
import com.dev.klvra.weatherapp.network.NetworkCallsHandler;
import java.util.Calendar;
import java.util.TimeZone;

public class WeatherActivity extends AppCompatActivity implements DBCallback {

    Toolbar toolbar;
    CitiesListFragment citiesListFragment;
    DBAccess dbAccess;
    NetworkCallsHandler networkCallsHandler;
    Bundle bundle;
    int min, hour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dbAccess = DBAccess.getInstance(this);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+3:00"));
        hour = calendar.get(Calendar.HOUR);
        min = calendar.get(Calendar.MINUTE);
        Log.d("MyTag", "Time is " + hour +":" + min);
        networkCallsHandler = new NetworkCallsHandler(this);
        citiesListFragment = new CitiesListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.cities_container, citiesListFragment, null).commit();
        dbAccess.open();
        dbAccess.close();
    }


    public void openInfoAboutCity(String name){
        Log.d("MyTag", "openInfo about " + name);
        dbAccess.open();
        CityDetailedInfo cityDetailedInfo = dbAccess.getDetailedInfo(name);
        dbAccess.close();
        String cityname = cityDetailedInfo.getCityname();
        Integer temp = cityDetailedInfo.getTemperature();
        Integer humidity = cityDetailedInfo.getHumidity();
        Integer pressure = cityDetailedInfo.getPressure();
        Integer tempmin = cityDetailedInfo.getTempmin();
        Integer tempmax = cityDetailedInfo.getTempmax();
        Integer windSpeed = cityDetailedInfo.getWindSpeed();
        Integer windDir = cityDetailedInfo.getWindDir();
        String clouds = cityDetailedInfo.getClouds();
        String image = cityDetailedInfo.getImage();
        bundle = new Bundle();
        bundle.putInt("humidity", humidity);
        bundle.putInt("temperature", temp);
        bundle.putInt("pressure", pressure);
        bundle.putInt("tempmin", tempmin);
        bundle.putInt("tempmax",tempmax);
        bundle.putString("clouds", clouds);
        bundle.putInt("windspeed", windSpeed);
        bundle.putInt("winddir", windDir);
        bundle.putString("cityname", cityname);
        bundle.putString("image", image);
        InfoAboutWeatherFragment fragment = new InfoAboutWeatherFragment();
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.cities_container, fragment).addToBackStack(null).commit();

    }

    @Override
    public void onDBFilled(boolean isFilled) {
        if (isFilled){
            citiesListFragment.refreshData();
        }
    }

    public int countCurrentHour(){
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+3:00"));
        hour = calendar.get(Calendar.HOUR);
        return hour;
    }

    public int countCurrentMinute(){
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+3:00"));
        min = calendar.get(Calendar.MINUTE);
        return min;
    }
}
