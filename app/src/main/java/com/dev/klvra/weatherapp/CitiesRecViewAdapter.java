package com.dev.klvra.weatherapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dev.klvra.weatherapp.model.CityOnFirstList;
import java.util.ArrayList;


public class CitiesRecViewAdapter extends RecyclerView.Adapter<CitiesRecViewAdapter.CityViewHolder>  {
    private Context context;
    private ArrayList<CityOnFirstList> list;

    public CitiesRecViewAdapter (Context context, ArrayList<CityOnFirstList> list){
        this.list = list;
        this.context = context;
        Log.d("MyTag", "Adapter created");
    }

    public class CityViewHolder extends RecyclerView.ViewHolder {

        protected TextView cityname;
        protected TextView temperature;
        protected TextView blankview;

        public CityViewHolder (View view){
            super(view);
            this.cityname = (TextView) view.findViewById(R.id.cityNameGeneral);
            this.temperature = (TextView) view.findViewById(R.id.temperatureGeneral);
            this.blankview = (TextView) view.findViewById(R.id.blankView);
        }
    }


    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rec_view_list_item_general, null);
        //Log.d("MyTag", "onCreateVH");
        CityViewHolder viewHolder = new CityViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
                //Log.d("MyTag", "onBindVH" + position);
                CityOnFirstList city = list.get(position);
                holder.cityname.setText(city.getCityName());
                holder.temperature.setText(city.getTemperature().toString() + "°C");
                holder.cityname.setOnClickListener(clickListener);
                holder.blankview.setOnClickListener(clickListener);
                holder.cityname.setTag(holder);
                holder.blankview.setTag(holder);
    }

    @Override
    public int getItemCount() { return list.size(); }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CityViewHolder holder = (CityViewHolder) v.getTag();
            int position = holder.getAdapterPosition();
            String item = list.get(position).getCityName();
            Log.d("MyTag", item);
            ((WeatherActivity) context).openInfoAboutCity(item);
        }
    };
}

