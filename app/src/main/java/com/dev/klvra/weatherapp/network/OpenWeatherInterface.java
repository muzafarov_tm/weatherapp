package com.dev.klvra.weatherapp.network;


import com.dev.klvra.weatherapp.model.ResponseWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface OpenWeatherInterface {



    @GET
    Call<ResponseWeather> getWeatherIn (@Url String URL);
}
