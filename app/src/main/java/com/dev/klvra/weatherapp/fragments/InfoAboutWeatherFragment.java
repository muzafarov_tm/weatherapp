package com.dev.klvra.weatherapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.klvra.weatherapp.R;
import com.squareup.picasso.Picasso;

public class InfoAboutWeatherFragment extends Fragment {

    TextView humidityView, pressureView, cloudsView, tempView, tempDeltaView, windView;
    ImageView imageView;

    Integer humidity;
    Integer pressure;
    String clouds;
    Integer temp;
    Integer tempmin;
    Integer tempmax;
    Integer windSpeed;
    Integer windDir;
    String windDirection;
    String cityname;
    String image;
    String imagePath;

    private final static String ICON_ADDR = "http://openweathermap.org/img/w/";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        humidity = bundle.getInt("humidity");
        pressure = bundle.getInt("pressure");
        clouds = bundle.getString("clouds");
        temp = bundle.getInt("temperature");
        tempmin = bundle.getInt("tempmin");
        tempmax = bundle.getInt("tempmax");
        windSpeed = bundle.getInt("windspeed");
        windDir = bundle.getInt("winddir");
        cityname = bundle.getString("cityname");
        image = bundle.getString("image");
        imagePath = ICON_ADDR + image +".png";

        temp = temp / 10;
        tempmax = tempmax / 10;
        tempmin = tempmin / 10;
        if (windDir < 90 && windDir > 0) windDirection = "NE";
        else if (windDir < 180 && windDir > 90) windDirection = "SE";
        else if (windDir > 180 && windDir < 270) windDirection = "SW";
        else if (windDir > 270 && windDir < 360) windDirection = "NW";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_city_weather, null);
        humidityView = (TextView) v.findViewById(R.id.humidityView);
        pressureView = (TextView) v.findViewById(R.id.pressureView);
        cloudsView = (TextView) v.findViewById(R.id.cloudlinessView);
        tempView = (TextView) v.findViewById(R.id.temperatureView);
        tempDeltaView = (TextView) v.findViewById(R.id.temperatureDeltaView);
        windView = (TextView) v.findViewById(R.id.windView);
        imageView = (ImageView) v.findViewById(R.id.imageView);
        Picasso.with(getActivity()).load(imagePath)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .into(imageView);
        Log.d("MyTag",imagePath);

        humidityView.setText(humidity + " %");
        pressureView.setText(pressure + " hPa");
        cloudsView.setText(clouds);
        tempView.setText(temp + " °C");
        if ((tempmax-tempmin)<2) tempDeltaView.setText("~" + (tempmin+tempmax)/2 + "°C");
        else tempDeltaView.setText("from " + tempmin + "°C to " + tempmax + " °C");
        windView.setText(windDirection + ", " + windSpeed + " m/s");
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(cityname);
    }
}
