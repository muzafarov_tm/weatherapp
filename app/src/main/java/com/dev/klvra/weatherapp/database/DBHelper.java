package com.dev.klvra.weatherapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {super(context, "weatherDB", null, 1);}

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE detailedinfo ("
                + "cityname TEXT, "
                + "temperature INTEGER, "
                + "pressure INTEGER, "
                + "humidity INTEGER, "
                + "tempmin INTEGER, "
                + "tempmax INTEGER, "
                + "clouds TEXT, "
                + "windspeed INTEGER, "
                + "winddir INTEGER, "
                + "image TEXT" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
