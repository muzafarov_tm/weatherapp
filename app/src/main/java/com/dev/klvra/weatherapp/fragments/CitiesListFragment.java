package com.dev.klvra.weatherapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.dev.klvra.weatherapp.CitiesRecViewAdapter;
import com.dev.klvra.weatherapp.R;
import com.dev.klvra.weatherapp.database.DBAccess;
import com.dev.klvra.weatherapp.model.CityOnFirstList;
import com.dev.klvra.weatherapp.network.NetworkCallsHandler;

import java.util.ArrayList;

public class CitiesListFragment extends Fragment {
    DBAccess dbAccess;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ArrayList<CityOnFirstList> list;
    NetworkCallsHandler networkCallsHandler;
    CitiesRecViewAdapter adapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MyTag","Fragment created");
        networkCallsHandler = new NetworkCallsHandler(getActivity());
        dbAccess = DBAccess.getInstance(getActivity());
        dbAccess.open();
        list = dbAccess.getCitiesList();
        dbAccess.close();
        if(list.size()==0) {
          networkCallsHandler.fillDB();
        }

        Log.d("MyTag", list.size() + "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cities_list, null);
        recyclerView = (RecyclerView) v.findViewById(R.id.cities_recycler_view);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if(list.size()==0){
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

        }
        else{
            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
        return v;

    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("MyTag", "OnActCreated");
        adapter = new CitiesRecViewAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Список");
    }

    public void refreshData(){
        dbAccess.open();
        list = dbAccess.getCitiesList();
        dbAccess.close();
        if(list.size()!=0) {
            adapter = new CitiesRecViewAdapter(getActivity(), list);
            adapter.notifyDataSetChanged();
            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

}
