package com.dev.klvra.weatherapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dev.klvra.weatherapp.DBCallback;
import com.dev.klvra.weatherapp.model.CityDetailedInfo;
import com.dev.klvra.weatherapp.model.CityOnFirstList;

import java.util.ArrayList;


public class DBAccess {

    private final String DETAILED_INFO_TABLE = "detailedinfo";

    private DBHelper dbHelper;
    private SQLiteDatabase database;
    private static DBAccess instance;
    private DBCallback callback;

    private DBAccess(Context context) {
        this.dbHelper = new DBHelper(context);
        this.callback = (DBCallback) context;
    }

    public static DBAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DBAccess(context);
        }
        return instance;
    }

    public void open() {this.database = dbHelper.getWritableDatabase();}

    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public ArrayList<CityOnFirstList> getCitiesList(){
        ArrayList<CityOnFirstList> list = new ArrayList<>();
        String columns[] = {"cityname", "temperature"};
        Cursor cursor = database.query(DETAILED_INFO_TABLE, columns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String cityName = cursor.getString(cursor.getColumnIndex("cityname"));
            Integer temperature = cursor.getInt(cursor.getColumnIndex("temperature"));
            CityOnFirstList city = new CityOnFirstList(cityName, temperature);
            list.add(city);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public CityDetailedInfo getDetailedInfo(String city){
        String Squery = "SELECT * from " + DETAILED_INFO_TABLE + " WHERE cityname = ?";
        String city_array[] = {city};
        Cursor cursor = database.rawQuery(Squery, city_array);
        Log.d("MyTag", cursor.getCount() + " cursor size");
        cursor.moveToFirst();
            String cityName = cursor.getString(cursor.getColumnIndex("cityname"));
            Integer temperature = cursor.getInt(cursor.getColumnIndex("temperature"));
            Integer tempmin = cursor.getInt(cursor.getColumnIndex("tempmin"));
            Integer tempmax = cursor.getInt(cursor.getColumnIndex("tempmax"));
            Integer pressure = cursor.getInt(cursor.getColumnIndex("pressure"));
            Integer humidity = cursor.getInt(cursor.getColumnIndex("humidity"));
            String clouds = cursor.getString(cursor.getColumnIndex("clouds"));
            Integer windSpeed = cursor.getInt(cursor.getColumnIndex("windspeed"));
            Integer windDir = cursor.getInt(cursor.getColumnIndex("winddir"));
            String image = cursor.getString(cursor.getColumnIndex("image"));
            Log.d("MyTag","String from cursor " + image);
            CityDetailedInfo cityDetailedInfo = new CityDetailedInfo(cityName, temperature, tempmin,
                    pressure, tempmax, humidity, windSpeed, windDir, clouds, image);
            cursor.close();
        return cityDetailedInfo;

    }

    public void addOrRefreshInfoInDB (CityDetailedInfo cityinfo){
        String city = cityinfo.getCityname();
        String type_array[] = {city};
        ContentValues cv = new ContentValues();
        cv.put("temperature", cityinfo.getTemperature());
        cv.put("tempmin", cityinfo.getTempmin());
        cv.put("tempmax", cityinfo.getTempmax());
        cv.put("pressure", cityinfo.getPressure());
        cv.put("humidity", cityinfo.getHumidity());
        cv.put("clouds", cityinfo.getClouds());
        cv.put("windspeed", cityinfo.getWindSpeed());
        cv.put("winddir", cityinfo.getWindDir());
        cv.put("image", cityinfo.getImage());
        Cursor cursor = database.query(DETAILED_INFO_TABLE, null, "cityname = ?", type_array, null, null, null);
        if(cursor.getCount()!=0) {
            Log.d("MyTag", "Update DB " + "DET_INFO " + cityinfo.getCityname());
            database.update(DETAILED_INFO_TABLE, cv, "cityname = ?", type_array);
        }
        else {
            cv.put("cityname", cityinfo.getCityname());
            database.insert(DETAILED_INFO_TABLE, null, cv);
            Log.d("MyTag", "Insert into DB " + "GEN_INFO " + cityinfo.getCityname());
        }
    }

}
